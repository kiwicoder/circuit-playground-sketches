// Circuit Playground Switches
//
// Simple demo showing turning lights on and off as you press switches.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
}

bool leftOn = false;
bool rightOn = false;

void loop() {
  // See what the buttons are currently
  bool leftFirstPressed  = CircuitPlayground.leftButton();
  bool rightFirstPressed = CircuitPlayground.rightButton();

  // Debounce delay 1/100s (10 milliseconds)
  delay(10);

  // Now check for buttons that were released.
  bool leftSecondPressed  = CircuitPlayground.leftButton();
  bool rightSecondPressed = CircuitPlayground.rightButton();

  // See if the left button has been released
  if (leftFirstPressed && !leftSecondPressed) {
    leftOn = !leftOn;
  }

  // See if the right button has been released
  if (rightFirstPressed && !rightSecondPressed) {
    rightOn = !rightOn;
  }

  // If the left button has been pressed then turn on the left
  // side LEDs
  if (leftOn) {
    CircuitPlayground.setPixelColor(0, 255,   0,   0); // Red
    CircuitPlayground.setPixelColor(1,   0, 255,   0); // Green
    CircuitPlayground.setPixelColor(2,   0,   0, 255); // Blue
    CircuitPlayground.setPixelColor(3, 255, 255, 128); // White
    CircuitPlayground.setPixelColor(4,   0, 255, 255); // Cyan
  } else {
    CircuitPlayground.setPixelColor(0,   0,   0,   0);
    CircuitPlayground.setPixelColor(1,   0,   0,   0);
    CircuitPlayground.setPixelColor(2,   0,   0,   0);
    CircuitPlayground.setPixelColor(3,   0,   0,   0);
    CircuitPlayground.setPixelColor(4,   0,   0,   0);
  }

  // If the right button has been pressed then turn on the right
  // side LEDs
  if (rightOn) {
    CircuitPlayground.setPixelColor(5, 192, 192, 192); // Silver
    CircuitPlayground.setPixelColor(6, 255, 255,   0); // Yellow
    CircuitPlayground.setPixelColor(7, 255,   0, 255); // Magenta
    CircuitPlayground.setPixelColor(8, 210, 105,  30); // Brown
    CircuitPlayground.setPixelColor(9, 128,   0, 128); // Purple
  } else {
    CircuitPlayground.setPixelColor(5,   0,   0,   0);
    CircuitPlayground.setPixelColor(6,   0,   0,   0);
    CircuitPlayground.setPixelColor(7,   0,   0,   0);
    CircuitPlayground.setPixelColor(8,   0,   0,   0);
    CircuitPlayground.setPixelColor(9,   0,   0,   0);
  }
}

