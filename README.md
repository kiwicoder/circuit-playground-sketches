# Circuit Playground Sketches

## Description
This is a set of sketches for the AdaFruit Circuit Playground. They have been created to give an idea of the capabilities of the circuit for people who are new to the whole Arduino world.

Feel free to have a play and let me know if you find any issues.

## Setup
These have all been tested on Windows 8, running Arduino Studio 1.8.2.
The only extra library installed is the SleepyDog library mentioned below.

## Credit
These sketches derived inspiration from the great work done by the Guys over at AdaFruit, find their code (with useful examples) at [Circuit Playground](https://github.com/adafruit/Adafruit_CircuitPlayground) & [SleepyDog (Watchdog)](https://github.com/adafruit/Adafruit_SleepyDog).

## License
All this code is licensed using the MIT License (see LICENSE.md for the text).
