// Circuit Playground White LED Chaser
//
// Have a single white LED go round in circles forever.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
}

int led_off = 9; // This LED will be turned off
int led_on  = 0; // This LED will be turned on

void loop() {
  // Turn the LED on and off
  CircuitPlayground.setPixelColor(led_off,   0,   0,   0);
  CircuitPlayground.setPixelColor(led_on,  255, 255, 255);
  delay(500);

  // Increment the off led_number keeping it within 0-9.
  led_off++;
  if (led_off > 9) {
    led_off = 0;
  }

  // Increment the on led_number keeping it within 0-9.
  led_on++;
  if (led_on > 9) {
    led_on = 0;
  }
}
