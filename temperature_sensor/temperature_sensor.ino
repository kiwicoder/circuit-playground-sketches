// Circuit Playground Temperature Sensor Demo
//
// The circle of lights will display the current temperature.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

/*
 * Set the selected LED to the specified value, as long as the value is bigger or equal to the limit value
 * specified.
 */
void setLEDColour(uint8_t ledNumber, uint8_t value, uint8_t limit, uint8_t red, uint8_t green, uint8_t blue) {
  if (value >= limit) {
    CircuitPlayground.setPixelColor(ledNumber, red, green, blue);  
  }
}

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
}

void loop() {
  /*
   * Get the temperature from the on-board temperature sesnor.
   */
  uint8_t temperature = (uint8_t)CircuitPlayground.temperature();

  // Determine if the temperature is hot (above 0 degrees C)
  bool hot = temperature > 0;

  // If the temperature is below zero, convert it into a positive
  // number by multiply it by negative one.
  if (temperature < 0) {
    temperature = temperature * -1.0;
  }

  // Break down the temperature value into the ones and tens digits
  // (e.g. 14C becomes tensDigits=1 and onesDigits=4)
  uint8_t tensDigits = (uint8_t)(temperature / 10);
  uint8_t onesDigits = (uint8_t)(temperature % 10);

  // Send the temperature back to the computer using the serial port
  Serial.print(temperature);
  Serial.print(" ");
  Serial.print(tensDigits);
  Serial.print(" ");
  Serial.print(onesDigits);
  Serial.println(" ");

  CircuitPlayground.clearPixels();

  // Set the 10s digit display using LEDS 1-5
  setLEDColour(0, tensDigits, 1, hot? 255:0, 0, hot?0:255);
  setLEDColour(1, tensDigits, 2, hot? 255:0, 0, hot?0:255);
  setLEDColour(2, tensDigits, 3, hot? 255:0, 0, hot?0:255);
  setLEDColour(3, tensDigits, 4, hot? 255:0, 0, hot?0:255);
  setLEDColour(4, tensDigits, 5, hot? 255:0, 0, hot?0:255);

  /*
   * Set the 1s digit display using LEDS 1-9
   * Because we might overlap due to trying to use 5 LEDs
   * to show ten different values, we show a different
   * value by turning it purple.
   */ 
  setLEDColour(5, onesDigits, 1, hot? 255:0,   0, hot?0:255);
  setLEDColour(6, onesDigits, 2, hot? 255:0,   0, hot?0:255);
  setLEDColour(7, onesDigits, 3, hot? 255:0,   0, hot?0:255);
  setLEDColour(8, onesDigits, 4, hot? 255:0,   0, hot?0:255);
  setLEDColour(9, onesDigits, 5, hot? 255:0,   0, hot?0:255);
  setLEDColour(9, onesDigits, 6, hot? 255:128, 0, hot?128:255);
  setLEDColour(8, onesDigits, 7, hot? 255:128, 0, hot?128:255);
  setLEDColour(7, onesDigits, 8, hot? 255:128, 0, hot?128:255);
  setLEDColour(6, onesDigits, 9, hot? 255:128, 0, hot?128:255);

  delay(200);
}
