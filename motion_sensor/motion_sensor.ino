// Circuit Playground Motion Sensor Demo
//
// Change colors as the board is tilted and moved
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

// Define range of possible acceleration values.
#define MIN_ACCEL -10.0
#define MAX_ACCEL 10.0

// Define range of color values (min color and max color)
// First the min color values
#define MIN_COLOR_RED   0x00
#define MIN_COLOR_GREEN 0x00
#define MIN_COLOR_BLUE  0x00

// Then the max color values
#define MAX_COLOR_RED   0xFF
#define MAX_COLOR_GREEN 0xFF
#define MAX_COLOR_BLUE  0xFF

/*
 * Convert the input value (which will be between inputMin and inputMax) into an output value that will
 * be between outputMin and outputMax.
 */
float convertMotionToColor(float input, float inputMin, float inputMax, float outputMin, float outputMax) {
  if (input >= inputMax) {
    return outputMax;
  }
  if (input <= inputMin) {
    return inputMin;
  }
  return outputMin + (outputMax - outputMin) * ((input - inputMin) / (inputMax - inputMin));
}

// Set up the CircuitPlayground libraries, clear the LEDs and set the Accelerometer to sensitive (2G) mode
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
  CircuitPlayground.setAccelRange(LIS3DH_RANGE_2_G);
}

void loop() {
  // Get the X, Y & Z motions (-10.0 - 10.0) and convert then into color values (0 - 255)
  uint8_t red   = (uint8_t)convertMotionToColor(CircuitPlayground.motionX(), MIN_ACCEL, MAX_ACCEL, MIN_COLOR_RED,   MAX_COLOR_RED);
  uint8_t green = (uint8_t)convertMotionToColor(CircuitPlayground.motionY(), MIN_ACCEL, MAX_ACCEL, MIN_COLOR_GREEN, MAX_COLOR_GREEN);
  uint8_t blue  = (uint8_t)convertMotionToColor(CircuitPlayground.motionZ(), MIN_ACCEL, MAX_ACCEL, MIN_COLOR_BLUE,  MAX_COLOR_BLUE);

  // Send debug information to the computer using the Serial port
  Serial.print(CircuitPlayground.motionX());
  Serial.print(" -> ");
  Serial.print(red);
  Serial.print(" / ");
  Serial.print(CircuitPlayground.motionY());
  Serial.print(" -> ");
  Serial.print(green);
  Serial.print(" / ");
  Serial.print(CircuitPlayground.motionZ());
  Serial.print(" -> ");
  Serial.print(blue);
  Serial.println("");

  // Set all the LEDs to the same color
  CircuitPlayground.setPixelColor(0, red, green, blue);
  CircuitPlayground.setPixelColor(1, red, green, blue);
  CircuitPlayground.setPixelColor(2, red, green, blue);
  CircuitPlayground.setPixelColor(3, red, green, blue);
  CircuitPlayground.setPixelColor(4, red, green, blue);
  CircuitPlayground.setPixelColor(5, red, green, blue);
  CircuitPlayground.setPixelColor(6, red, green, blue);
  CircuitPlayground.setPixelColor(7, red, green, blue);
  CircuitPlayground.setPixelColor(8, red, green, blue);
  CircuitPlayground.setPixelColor(9, red, green, blue);
}
