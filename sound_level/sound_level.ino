// Circuit Playground Sound Sensor
//
// The circle of lights will become brighter as the sound
// gets louder.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
}

void loop() {
  /*
   * Step through the LEDs in a sequence 1-9
   */
  for (int ledNumber = 0; ledNumber != 10; ++ledNumber) {
    // Clear the current LED
    CircuitPlayground.setPixelColor(ledNumber, 0, 0, 0);
    
    // Delay for 1/10 minute (100 milliseconds)
    delay(100);

    // Get the sound level from the on-board sensor
    uint16_t soundLevel = CircuitPlayground.soundSensor();

    /*
     * The value from the sensor is between 0-1024, so we need to convert
     * it into a value between 0-254 for the LED value, so divide it by 4
     */
    uint16_t ledValue = soundLevel / 4;

    // Send the sound level back to the computer using the serial port
    Serial.print(soundLevel, DEC);
    Serial.print(" ");
    Serial.print(ledValue);
    Serial.println(" ");

    // Set the current LED to the calculated value
    CircuitPlayground.setPixelColor(ledNumber, ledValue, ledValue, ledValue);
  }
}
