// Circuit Playground Random Color LED Chaser v2
//
// The colors will change in a circle, one after another in a random color sequence.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

uint8_t led_number = 0;
uint8_t led_values[10][3];

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();

  // Set the LEDS to the initial set of random colors
  for (int temp = 0; temp != 10; temp++) {
    led_values[temp][0] = random(255);
    led_values[temp][1] = random(255);
    led_values[temp][2] = random(255);

    CircuitPlayground.setPixelColor(temp,  led_values[temp][0], led_values[temp][1], led_values[temp][2]);
  }
}

void loop() {
  // Change the values by a random amount - the value will be truncated to between 0-255
  led_values[led_number][0] = (led_values[led_number][0] + random(255));
  led_values[led_number][1] = (led_values[led_number][1] + random(255));
  led_values[led_number][2] = (led_values[led_number][2] + random(255));

  // Set the LED to the chosen colour sequence
  CircuitPlayground.setPixelColor(led_number,  led_values[led_number][0], led_values[led_number][1], led_values[led_number][2]);

  // Delay for 1/4s (250 milliseconds)
  delay(250);

  // Increment the led_number keeping it within 0-9.
  led_number++;
  if (led_number > 9) {
    led_number = 0;
  }
}
