// Circuit Playground Light Sensor
//
// The circle of lights will change as the light level changes.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

int ledNumber = 0;  // The current LED index
int colorIndex = 0; // The current colour index
uint8_t ledArray[10][3]; // An array holding the current values

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
}

void loop() {
  // Get the current light level from the on-board sensor
  uint16_t lightLevel = CircuitPlayground.lightSensor();

  // The light level will have a value between 0-1023, so 
  // we need to convert it to 0-254, so we divide it by 4
  uint8_t rgbValue = lightLevel / 4;

  // Set the current LED/color value
  ledArray[ledNumber][colorIndex] = rgbValue;

  // Set through all the LEDs (1-9) and set the RGB values to the current values from the array
  for (int i = 0; i != 10; i++) {
    CircuitPlayground.setPixelColor(ledNumber, ledArray[ledNumber][0], ledArray[ledNumber][1], ledArray[ledNumber][2]);
  }

  // Send the information back to the computer via Serial port
  Serial.print(rgbValue, DEC);
  Serial.print(" ");
  Serial.print(ledNumber, DEC);
  Serial.println(" ");

  // Move to the next LED in the sequence
  ledNumber++;
  if (ledNumber > 9) {
    // If we are now above the highest value (i.e. 9) then reset the
    // LED number back to zero
    ledNumber = 0;

    // Move to the next colour index
    colorIndex++;
    if (colorIndex > 2) {
      // If we are above the highest value (i.e. 2) then reset to zero
      colorIndex = 0;
    }
  }

  // Clear the LED value ... this will be the next LED to be set
  // because we incremented the LED number in the code above.
  CircuitPlayground.setPixelColor(ledNumber, 0, 0, 0);

  // Delay for 1/10s (100 milliseconds)
  delay(100);
}
