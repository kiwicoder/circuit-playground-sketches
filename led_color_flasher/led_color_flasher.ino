// Circuit Playground Color LED flasher
//
// Use the 10 NeoPixel RGB LEDs to flash a variety of colours.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
}

void loop() {
  /*
   * Turn on all the LEDs, each with a different colour
   */
  CircuitPlayground.setPixelColor(0, 255,   0,   0); // Red
  CircuitPlayground.setPixelColor(1,   0, 255,   0); // Green
  CircuitPlayground.setPixelColor(2,   0,   0, 255); // Blue
  CircuitPlayground.setPixelColor(3, 255, 255, 128); // White
  CircuitPlayground.setPixelColor(4,   0, 255, 255); // Cyan
  CircuitPlayground.setPixelColor(5, 192, 192, 192); // Silver
  CircuitPlayground.setPixelColor(6, 255, 255,   0); // Yellow
  CircuitPlayground.setPixelColor(7, 255,   0, 255); // Magenta
  CircuitPlayground.setPixelColor(8, 210, 105,  30); // Brown
  CircuitPlayground.setPixelColor(9, 128,   0, 128); // Purple
 
  // Sleep for 1 second (1000 milliseconds)
  delay(1000);

  // Turn all the LEDs off
  CircuitPlayground.clearPixels(); 

  // Sleep for 1 second (1000 milliseconds)
  delay(1000);
}
