// Circuit Playground Random Color LED Chaser
//
// The colors will change in a circle, one after another in a random color sequence.
//
// Author: Andrew Norman
// License: MIT License (https://opensource.org/licenses/MIT)

#include <Adafruit_CircuitPlayground.h>

// Set up the CircuitPlayground libraries and clear the LEDs
void setup() {
  CircuitPlayground.begin();
  CircuitPlayground.clearPixels();
}

uint8_t led_number = 0;

void loop() {
  // Change the LED to a random color
  CircuitPlayground.setPixelColor(led_number,  random(255), random(255), random(255));

  // Delay for 1/4s (250 milliseconds)
  delay(250);

  // Increment the led_number keeping it within 0-9.
  led_number++;
  if (led_number > 9) {
    led_number = 0;
  }
}
